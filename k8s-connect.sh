#!/bin/bash

context=$(kubectl config current-context)
user=$(echo $context | cut -d'@' -f1)
cluster=$(echo $context | cut -d'@' -f2)
client_certificate_data=$(yq eval ".users.[] | select(.name == \"$user\") | .user.client-certificate-data" ~/.kube/config)
client_key_data=$(yq eval ".users.[] | select(.name == \"$user\") | .user.client-key-data" ~/.kube/config)
culster_ca=$(yq eval ".clusters.[] | select(.name == \"$cluster\") | .*.certificate-authority-data" ~/.kube/config)
server=$(yq eval ".clusters.[] | select(.name == \"$cluster\") | .*.server" ~/.kube/config)

echo "####################################copy yaml below####################################"
echo "#@data/values"
echo "#@overlay/match-child-defaults missing_ok=True"
echo "---"
echo cluster_ca: "$culster_ca"
echo cluster_api: "$server"
echo cluster_name: "$cluster"
echo cluster_user_name: "$user"
echo context_name: "$context"
echo user_certificate_data: "${client_certificate_data}"
echo users_client_key: "${client_key_data}"
echo "####################################################################################"
