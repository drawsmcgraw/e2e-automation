# Setup GitLab CI

Once you have forked the project go into Settings > CI/CD in your Spring Music project

![Example](/docs/cicd.png)

Then click on runners

![Example](/docs/install-runner.png)

Integrate with a Kubernetes Cluster

![Example](/docs/k8s.png)

You have an existing cluster

![Example](/docs/existing-clust.png)

Go back to your install jumbox and run `./gitlab-info.sh` and populate the config

![Example](/docs/config.png)

You are connected, deploy a runner

Click on Appplications under you newly configured cluster

![Example](/docs/applications.png)

Click install on GitLab Runner, this is all you need for now

![Example](/docs/installer.png)

Validate your runner is online and turn off shared runners

![Example](/docs/installed-runner.png)

Create 3 secrets by clicking Settings > Variables in your Spring Music project

- Go back to your jump box and run `./k8s-connect.sh` copy the output of this command.
- make a file type variable with a name(key) that maps to a description of the cluster (e.g. AWS-DEMO-CLUSTER1) and copy the contents of the script output into the value
- Now create a variable with the name (key) `cluster_context_spec` and set the value to the name (key) of the previous file variable.
- Create a spring-music project in your harbor instance.
- Now create a variable with the name (key) EXT_REGISTRY_IMAGE and enter your harbor.ingress domain/spring-music.  

![Example](/docs/vars.png)


