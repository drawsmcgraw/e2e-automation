# Tanzu e2e Install Automation

## Background
Automate the install of common tooling for end2end demos.  This project assumes that you have a clean cluster with a bastion host that can
run docker and assorted bash scripts.  This has been tested with the standard Ubuntu bastion host provisioned by TKG.

This does install a number of CLI tools, etc so it is advised that you install this on a jumpbox or ephemeral host.  We can move this to a container if it proves useful.

Tools:

- Tanzu Build Service - Automated Container builds
- Elastic+fluentbit+kibana - log aggregation
- Prometheus+Grafana - metrics/observability
- Tanzu SQL PostgreSQL Operator - SQL DB
- Harbor Image Registry - Image Registry, Scanning, etc.
- SonarQube - SAST, Code Quality
- kubeapps - self service middleware 
- Contour - Ingress Controller
- Cert Manager - generation of TLS certs for ingress, etc.

(Instructions for connecting gitlab runner, if desired)

With TKGm use k8s version v1.18.8+vmware.1 or older as of now to avoid containerd incompatibility with TBS.

With TKGs make sure that you have configured enough ephemeral space for your worker nodes

```yaml
...
volumes:
        - name: containerd-lib
          capacity: 
            storage: 300G
          mountPath: /var/lib/containerd/

```

## Pre-install
```bash
sudo apt install docker.io git

sudo groupadd docker

sudo usermod -aG docker ubuntu

newgrp docker 
```

## Run install

```bash

git clone https://gitlab.com/tanzu-e2e/e2e-automation.git

#make sure you have kubectl access to your cluster
#Copy your .kube/config file up to the jump box as an easy way to get kubectl hooked up

#Create a values.yaml to configure your install
#there is a values.yaml.example file included to get started, do not add extraneous comments to the yaml

./env-install.sh

```

## Run the pipeline

[Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) the spring music app in this group (https://gitlab.com/tanzu-e2e/spring-music)



[Setup GitLab CI/CD](docs/gitlab_setup.md)

![Example](/docs/pipe.png)

